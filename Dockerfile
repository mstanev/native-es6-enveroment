FROM halverneus/static-file-server:latest

MAINTAINER Milen Stanev <milen.stanev@gmail.com>
COPY . /web
EXPOSE 8080
