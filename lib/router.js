/**
 * Router
 */
import { VIEWS_BASE, MAIN_ELEMENT } from '../constants.js';

/**
 * Routes collection
 * * Add your modules in the collection
 * @example
 * routes.push({
      path: '/',
      view: './controllers/movieSearch.js',
   })
 * @type {{view: string, path: string}[]}
 */
export let router = [];

/**
 * 404 Page
 * @type {{mainElement: string, pageNotFound: string}}
 */
export const settings = {
  mainElement: document.getElementById(MAIN_ELEMENT),
  pageNotFound: `${VIEWS_BASE}/404.js`,
};

/**
 * @type {{viewModule: ContainerBase}}
 */
const viewWrapper = { viewInstance: undefined };

/**
 * Handle route hash change and load view
 */
const loadView = () => {
  let viewPath = settings.pageNotFound;
  const currentHash = location.hash.slice(1).toLowerCase();
  const routeResource = router.find(routeItem => {
    if (routeItem.path.split('/')[1] === currentHash.split('/')[1]) {
      return routeItem;
    }
  });

  if (routeResource) {
    viewPath = routeResource.view;
  }

  import(viewPath).then(
    (viewModule, data = {}) => {
      try {
        // Unsubscribe Module Events, It's defined in {ContainerBase}
        viewWrapper.viewInstance.unsubscribeModule();
      } catch (e) {}
      delete viewWrapper.viewInstance;

      viewWrapper.viewInstance = new viewModule.default(
        data,
        settings.mainElement,
      );
    },
    error => console.error(`Error: ${error} \nBad module path`),
  );
};

/**
 * Attach events and initialize the router
 */
/*window.addEventListener('load', () => {
  loadView();
  window.addEventListener('hashchange', loadView);
});*/

class Router extends HTMLElement {
  static settings = {
    pageNotFound: `${VIEWS_BASE}/404.js`,
  };
  static style = {
    display: 'block',
    minHeight: '100vh',
  };
  get routes() {
    return this._routes;
  }
  set routes(args) {
    this._routes = [...args];
  }

  constructor() {
    super();
    this._routes = [];
    // TODO: what for is it?
    Object.keys(this.constructor.style).forEach(propName => {
      this.style[propName] = this.constructor.style[propName];
    });
  }

  loadView(path) {
    const { pageNotFound } = this.constructor.settings;
    const currentHash = path.slice(1).toLowerCase();
    const routeResource = this.routes.find(routeItem => {
      if (routeItem.path.split('/')[1] === currentHash.split('/')[1]) {
        return routeItem || pageNotFound;
      }
    });

    const success = module => {
      this.constructor._clean.call(this);
      this.constructor._inject.call(this, module.default);
    };
    const error = error => console.error(`Error: ${JSON.stringify(error)} \nBad module path`);
    import(routeResource.view).then(success, error);
  }

  static _clean() {
    if (this.viewModule) {
      this.viewModule.disconnectedCallback();
      delete this.viewModule;
    }
  }

  static _inject(module) {
    // TODO: validation about is it instance of {ContainerBase}
    this.viewModule = new module(this);
    this.viewModule.connectedCallback();
  }

  /**
   * Load initial view
   * Listen for hash change and do routing
   */
  connectedCallback() {
    let hash = window.location.hash;

    if (hash === '') {
      window.location.hash = '/';
    }
    if (this.routes.length) {
      this.loadView(hash);
    }
    window.addEventListener(
      'hashchange',
      () => this.loadView(window.location.hash),
    );
  }

  disconnectedCallback() {
    window.removeEventListener('hashchange', () => {
      this.routes = [];
    });
  }
}
customElements.define('app-router', Router);
