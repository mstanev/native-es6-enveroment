export default class PubSub {
  constructor() {
    this.events = {};
    //TODO: env
    if (!window.env) {
      try {
        window.events = this.events;
      } catch (e) {
        console.error(new Error('Variable with name "events" is already defined.'));
      }
    }
  }

  /**
   * @param event - Event name
   * @param callback - Callback
   * @param subscriber - Subscriber
   */
  subscribe(event, callback, subscriber) {
    let self = this;

    if(!self.events.hasOwnProperty(event)) {
      self.events[event] = [];
    }

    self.events[event].push({
      callback, subscriber
    });
  }

  unsubscribe(subscriberToRemove, event = '') {
    this.events[event].forEach(({subscriber}, index) => {
      if (subscriber === subscriberToRemove) {
        this.events[event].splice(index, 1);
      }
    });
  }

  publish(event, data = {}) {
    if(this.events.hasOwnProperty(event)) {
      this.events[event].forEach(({callback}) => callback(data));
      // console.info(event, data);
    }
  }
}
