import Store from './store.js';

let state = {};

class ContainerBase {
  static store = {};
  static props = {};
  static actions = {};

  get props() {
    return this.constructor.props;
  }
  get store() {
    return { ...this.constructor.store };
  }
  get state() {
    return { ...state };
  }
  set state(args) {
    throw new Error('Not allowed');
  }

  /**
   * @param {Store} store
   * @param {HTMLElement} $placeholderElement
   */
  constructor(store, $placeholderElement) {
    this.$viewPlaceholder = $placeholderElement;
  }

  connectedCallback() {
    const {actions, props, mapContainerActions, mapContainerProps} = this.constructor;

    mapContainerActions(actions, props);
    this.store.events.subscribe(
      'stateChange',
      () => {
        if (JSON.stringify(this.state) !== JSON.stringify(this.store.state)) {
          state = { ...this.store.state };
          mapContainerProps(this.state, props);
          this.render();
        }
      },
      this.constructor.name,
    );
    this.render();
  }

  disconnectedCallback() {
    this.store.events.unsubscribe(this.constructor.name, 'stateChange');
  }

  /**
   * Basic Render
   * Will be called on each state change
   */
  render() {
    const template = this.template();

    if (this.$viewPlaceholder.innerHTML !== template) {
      this.$viewPlaceholder.innerHTML = template;
    }

    this.constructor.mapChildProps(
      this.$viewPlaceholder,
      this.constructor.actions,
      this.constructor.props
    );
  }

  //region static
  /**
   * Check if the props exist in store
   * @param store
   */
  static validateProps(store) {
    const propsArray = Object.keys(this.props);

    if (propsArray.length) {
      propsArray.forEach((prop, index) => {
        if (!store.state.hasOwnProperty(prop)) {
          console.warn(
            `prop "${prop}" missing in the state:`,
            JSON.stringify(store.state),
          );
        }
      });
    }
    /*throw new Error(
      `Container props are not defined in container instance "${this.constructor.name}"`,
    );*/
  }

  static mapContainerActions(actions, props) {
    Object.keys(actions).forEach(itemName => {
      props[itemName] = actions[itemName];
    });
  };

  static mapContainerProps(state, props) {
    Object.keys(props).map(itemName => {
      if (!state.hasOwnProperty(itemName)) {
        console.warn(
          `prop "${itemName}" missing in the state:`,
          JSON.stringify(state),
        );
      }
      props[itemName] = state[itemName];
    });
  };

  static mapChildProps($viewPlaceholder, actions, props) {
    $viewPlaceholder.childNodes.forEach(item => {
      if (item.constructor.hasOwnProperty('props')) {
        item.constructor.props.forEach(name => {
          item._props[name] =
            actions[name] || props[name];
        });
      }
    });
  }
  //endregion

  //region abstract methods
  /**
   * @abstract
   */
  template() {
    throw new Error('Template is required!');
    return `template string`;
  }
  //endregion
}

export default ContainerBase;
