const templatePath = 'asdasd';

/**
 * ComponentBase extend HTMLElement
 */
class ComponentBase extends HTMLElement {
  get props() {
    return this._props;
  }

  constructor(...args) {
    super();
    this._props = {};
    this.bootstrap(...args);
  }

  /**
   * @param templateUrl
   */
  bootstrap(templateUrl = '') {
    if (templateUrl) {
      this.loadTemplate(templateUrl)
        .then(() => this.onTemplateLoaded());
    }
  }

  /**
   * @abstract
   */
  onTemplateLoaded() {}

  /**
   * @param url {String} - Template path
   * @returns {Promise<void>}
   */
  async loadTemplate(url) {
    let response = await fetch(url);
    this.innerHTML = await response.text();

    /*const tpl = document.importNode(
      document.querySelector('#tpl').content,
      true,
    );
    tpl.querySelector('h2').textContent = url;
    tpl.querySelector('img').remove();
    this.appendChild(tpl);*/
  }
}

export default ComponentBase;
