import PubSub from './pubsub.js';

/**
 * Prepare Proxy for state
 * @param state
 * @returns {Proxy}
 */
function connectProxy(state) {
  const self = this;

  return new Proxy((state), {
    set(state, prop, value) {
      if (JSON.stringify(value) !== JSON.stringify(state[prop])) {
        state[prop] = value;
        self.events.publish('stateChange', self.state);
      }
      return true;
    },
  });
}

/**
 * Description
 * @example
 *  {{
 *    mutations: {
 *      apiError,
 *      loadData
 *    },
 *    state: {data},
 *    actions: {resetStore, loadData}
 *  }}
 */
class Store {
  constructor({ actions, mutations, state }) {
    if (!actions || !mutations || !state) {
      console.error(
        new Error(`Unassigned crucial parameter {actions, mutations, state}`)
      );
    }
    this.events = new PubSub();
    this.actions = actions;
    this.mutations = mutations;
    this.state = connectProxy.call(this, state);
  }

  /**
   * Store Dispatcher
   * @param actionKey - Action Name
   * @param payload - Action Parameters
   * @returns {boolean}
   */
  dispatch(actionKey, payload) {
    if(typeof this.actions[actionKey] !== 'function') {
      console.error(`Action "${actionKey} doesn't exist.`);
      return false;
    }
    console.info(`Action: ${actionKey}\n`, payload);
    this.actions[actionKey](this, payload);
    return true;
  }

  /**
   * Commit Mutation
   * @param mutationKey - Mutation Name
   * @param payload - Data about mutation
   * @returns {boolean}
   */
  commit(mutationKey, payload) {
    if(typeof this.mutations[mutationKey] !== 'function') {
      console.log(`Mutation "${mutationKey}" doesn't exist`);
      return false;
    }
    console.info(`Mutation: ${mutationKey}\n`, `Payload: `, payload);
    this.state = this.mutations[mutationKey](this.state, payload);

    return true;
  }
}

export default Store;
