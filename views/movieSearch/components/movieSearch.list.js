import ComponentBase from '../../../lib/componentBase.js';

const List = data => data.length && data.map(item => `
    <li>
      <a href="#/movie-details/${item.imdbID}">
          ${item.Title}<br>
          <img alt="${item.Title}" src="${item.Poster}" />
      </a>
    </li>
`).join('');

const NoRecords = () => `<li>No Records</li>`;

class MovieSearchList extends ComponentBase {
  static props = ['data'];

  set data(data) {
    this.init(data);
  }

  constructor() {
    super();
    this.style.visibility = 'hidden';
    this._data = [];
  }

  connectedCallback() {
    if (this.dataset.hasOwnProperty('list')) {
      this.data = JSON.parse(this.dataset.list);
    }
  }

  setData(data) {
    this.init(data);
  }

  init(data = []) {
    this.innerHTML = `<ul>${List(data) || NoRecords()}</ul>`;
    this.style.visibility = 'visible';
  }
}

customElements.define('movie-list', MovieSearchList);
