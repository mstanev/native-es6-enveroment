import store from '../store/movieSearch.store.js';
import ComponentBase from '../../../lib/componentBase.js';

const templatePath = 'views/movieSearch/components/movieSearch.searchForm.html';

class MovieSearchSearchForm extends ComponentBase {
  static props = ['searchMovie', 'searchValue'];

  constructor() {
    super(templatePath);
  }

  disconnectedCallback() {
    this.removeEventListener('click', () => {});
  }

  onTemplateLoaded() {
    const { searchMovie, searchValue } = this.props;
    const inputEl = this.querySelector('input');

    inputEl.value = searchValue;
    this.addEventListener('click', e => {
      if (e.target.tagName === 'BUTTON') {
        searchMovie({ search: inputEl.value });
      }
    });
  }
}

customElements.define('search-form', MovieSearchSearchForm);
