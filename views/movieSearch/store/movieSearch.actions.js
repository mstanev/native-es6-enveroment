import {
  LOAD_DATA,
  LOAD_DATA_ERROR,
  LOAD_DATA_SUCCESS,
  RESET_STATE,
} from './movieSearch.constants.js';
// xhost +

export default {
  [LOAD_DATA](context, payload) {
    const { search } = payload;

    context.commit(LOAD_DATA, search);
    fetch(`http://www.omdbapi.com/?s=${search}&page=1&apikey=407def7c`, {
      mode: 'cors',
    }).then(
      response => {
        response.json().then(res => {
          if (res.Response === 'True') {
            context.commit(LOAD_DATA_SUCCESS, res.Search);
          } else if (res.Response === 'False') {
            context.commit(LOAD_DATA_ERROR, res.Error);
          }
        });
      },
      error => {
        context.commit(LOAD_DATA_ERROR, error);
      },
    );
  },

  resetStore(context) {
    context.commit(RESET_STATE);
  },
};
