import initialState from './movieSearch.initialState.js';
import {
  LOAD_DATA,
  LOAD_DATA_ERROR,
  LOAD_DATA_SUCCESS,
  RESET_STATE,
} from './movieSearch.constants.js';


export default {
  [LOAD_DATA](state, payload) {
    state.data = [];
    state.searchValue = payload;
    state.searching = true;
    return state;
  },
  [LOAD_DATA_ERROR](state, error) {
    window.alert(error);
    state.searching = false;
    state.searchValue = '';
    return state;
  },
  [LOAD_DATA_SUCCESS](state, payload) {
    state.data = payload;
    state.searching = false;
    state.searchValue = '';
    return state;
  },

  [RESET_STATE](state) {
    state.data = [];
    return state;
  },
};
