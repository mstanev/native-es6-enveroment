import actions from './movieSearch.actions.js';
import mutations from './movieSearch.mutations.js';
import state from './movieSearch.initialState.js';
import Store from '../../../lib/store.js';

/**
 * @type {{mutations: {apiError, searching, loadData, searchValue}, state: {data, isSearching, searchValue}, actions: {resetStore, loadData}}}
 * @private
 * @typedef MovieSearchStore
 */
const _store = {
  actions,
  mutations,
  state,
};

/**
 * @type {Store}
 */
const store = new Store(_store);

export default store;
