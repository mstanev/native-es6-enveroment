const namespace = 'MOVIE_SEARCH';
export const LOAD_DATA = `${namespace}/LOAD_DATA`;
export const LOAD_DATA_ERROR = `${namespace}/LOAD_DATA_ERROR`;
export const LOAD_DATA_SUCCESS = `${namespace}/LOAD_DATA_SUCCESS`;
export const RESET_STATE = `${namespace}/RESET_STATE`;
