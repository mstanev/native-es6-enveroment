import ContainerBase from '../../lib/containerBase.js';
import store from './store/movieSearch.store.js';
import { LOAD_DATA, RESET_STATE } from './store/movieSearch.constants.js';
import './components/movieSearch.searchForm.js';
import './components/movieSearch.list.js';

const movieListId = 'movie-list';
const elements = {
  /** @returns {MovieList} */
  movieList() {
    return document.getElementById(movieListId);
  },
};

export default class MovieSearch extends ContainerBase {
  static store = store;
  static props = {
    data: [],
    isSearching: false,
    searchValue: '',
  };
  static actions = {
    searchMovie: searchValue => store.dispatch(LOAD_DATA, searchValue),
    resetState: () => store.dispatch('resetStore'),
  };

  constructor($placeholderElement) {
    super(store, $placeholderElement);
  }

  template() {
    const { data, isSearching, searchValue } = this.props;

    return `
      <search-form
        data-value='${searchValue}' 
       ></search-form>
      ${isSearching ? 'loading' : ''}
      <h4>
        With dataSet attribute
      </h4>
      <movie-list data-list='${JSON.stringify(data)}'></movie-list>
      <h4>
        With setData method
      </h4>
      <movie-list id='${movieListId}'></movie-list>
    `;
  }

  render() {
    super.render();
    const { data } = this.props;
    elements.movieList().setData(data);
  }
}
