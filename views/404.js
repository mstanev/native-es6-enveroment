import ContainerBase from '../lib/containerBase.js';

export default class NotFound extends ContainerBase {
  static props = {};
  constructor(...state) {
    super({}, ...state);
  }
  template() {
    return 'Page Not found';
  }
}
