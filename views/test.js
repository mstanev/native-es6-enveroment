import ContainerBase from '../lib/containerBase.js';

export default class Test extends ContainerBase {
  constructor(props) {
    super(props);
  }

  template() {
    return 'Test';
  }
}
