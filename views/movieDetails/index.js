import ContainerBase from '../../lib/containerBase.js';
import store from './store/index.js';

const RowElement = data =>
  Object.keys(data)
    .map(key => {
      return `<tr><td>${key}</td><td>${data[key]}</td></tr>`;
    })
    .join('');

export default class MovieDetails extends ContainerBase {
  static store = store;
  static props = {
    data: {},
  };
  constructor($placeholderElement) {
    super(store, $placeholderElement);
  }

  template() {
    const { data } = this.props;

    return `
      <h5>
        <a href="#/">Go to Search</a>
      </h5>
      <table class="table">
        <tbody>
            ${RowElement(data) || '<tr><td>No Records</td></tr>'}
        </tbody>
      </table>`;
  }

  connectedCallback() {
    super.connectedCallback();
    store.dispatch('loadData');
  }

  disconnectedCallback() {
    super.disconnectedCallback();
    store.commit('loadData', []);
  }
}
