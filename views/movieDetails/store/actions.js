export default {
  loadData(context, payload) {
    const query = location.hash.split('/')[2];


    fetch(`http://www.omdbapi.com/?i=${query}&apikey=407def7c`)
      .then(
        (response) => {
          response.json().then(
            res => {
              context.commit('loadData', res);
            },
          )
        },
        (error) => {
          context.commit('apiError', error);
        }
      );
  }
};
