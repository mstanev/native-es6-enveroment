export default {
  loadData(state, payload) {
    state.data = payload;

    return state;
  },
  apiError(state, error) {
    window.alert(error);
    state.data = [];

    return state;
  }
};
