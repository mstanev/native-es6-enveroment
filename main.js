/**
 * Bootstrap the application
 */
import { router } from './lib/router.js';
import { VIEWS_BASE } from './constants.js';

// Set default hash route
if (location.hash === '') location.hash = '/';

/*router.push(
  { path: '/', view: `${VIEWS_BASE}/movieSearch/movieSearch.js` },
  { path: '/movie-details', view: `${VIEWS_BASE}/movieDetails/movieSearch.js` },
  { path: '/test', view: `${VIEWS_BASE}/test.js` },
);*/
